#LOD EXtractor
A module for covering a fragment of a knowledge graph  in an OWL ontology.

#Dependencies
Java 8+, Jena 3.0, OWLAPI 5.0

# Command line run configuration 
Check out the project (clone the repository) and Build through Maven. Run the program with the following command

 ``` java -jar extractor-0.0.1- SNAPSHOT confoptions```  
 
 where the confoptions are (the order is mandatory): 
 
    - online, true  to query an endpoint (e.g. Virtuoso)  false for local ontologies/RDF datasets
	- url, the endpoint (e.g "https://dbpedia.org/sparql") ;
    - nOfClasses, per level of the hierarchy;
	- depth, hierarchy  depth
	- nOfInstances= per class
	- nOfObjectProperties per class
	- nOfObjectPropertiesAssertions per property
	- nOfDataProperties per class
	- nOfDataPropertiesAssertion per property;
	- topConcept, url of the seed for extraction processs (e.g "http://dbpedia.org/ontology/Eukaryote")  or "owl:Thing"
	- kg,  the knowledge graph name (currently supported DBPEDIA, YAGO, OTHER) for parsing  the url. OTHER must be used only in off-line mode
	- fileIRI, to store the results ("file:////C:/Users/Giuseppe/Desktop/Ontologies/ontoEukaryot.owl")

	
 