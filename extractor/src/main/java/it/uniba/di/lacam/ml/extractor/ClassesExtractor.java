package it.uniba.di.lacam.ml.extractor;

import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;

import it.uniba.di.lacam.ml.extractor.utils.QueryExecutor;
import it.uniba.di.lacam.ml.extractor.utils.Triple;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.QuerySolution;

/**
 * A class to extract the concepts and the related hierarchy from  a KG
 * @author Giuseppe
 *
 */
public class ClassesExtractor {

	private QueryExecutor  qe ;
	private int nOfClasses; // per depth
	private int depth;
	// for recursive queries
	public ClassesExtractor(QueryExecutor qe, int nOfClasses, int depth) {
		this.qe=qe;
		this.nOfClasses=nOfClasses;
		this.depth= depth;
	}


	/**
	 * Extract the direct subclasses of the owl:Thing
	 * @return
	 */
	public List<QuerySolution> retrieveClasses() {
		String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> SELECT DISTINCT * WHERE { ?Concept rdfs:subClassOf owl:Thing}";
		return qe.executeQuery(query);
	} 

	//	public List<Triple> retrieveSubClassesTriples() {
	//		String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> SELECT DISTINCT * WHERE { ?Concept a owl:Class}";
	//
	//		List<Triple> executeQuery = new ArrayList<Triple>();
	//		List<QuerySolution> res=qe.executeQuery(query);
	//
	//		for (QuerySolution querySolution : res) {
	//
	//
	//		Triple t= new Triple((querySolution.getResource("?Concept").toString()),"a", "owl:Class");
	//		executeQuery.add(t);
	//		
	//		}
	//
	//		//System.err.println(executeQuery);
	//		return executeQuery;
	//	}

/**
 * 
 * @param inclusions, the set of the triples to be filled with the query results
 */
	public void retrieveSubClassesTriples( List<Triple> inclusions){
		if (Configuration.topConcept.compareTo("owl:Thing")!=0)
			inclusions.add(new Triple (Configuration.topConcept, "rdfs:subClassOf", "owl:Thing"));
		retrieveSubClassesTriples (inclusions, Configuration.topConcept, depth);



	}

	/**
	 * Extract the classes at i-th level of the hierarchy
	 * @param inclusions
	 * @param q, the concept whose subclasses are extracted
	 * @param i, the maximum depth of the hierarchy
	 */
	public void retrieveSubClassesTriples( List<Triple> inclusions,String q, int i){

		String query = composeQuery(q);
		System.out.println("Subclasses of "+q); 
		//if (i>=depth && i >=0) {
		//System.out.println(i +"vs"+ depth);


		// for the first level
		List<QuerySolution> res = qe.executeQuery(query);
		//System.out.println("is closed? );
		List<Triple> tripleofCurrentLevel = new ArrayList<Triple>(); //store the triples of the current levels
		List<Triple> triplePreviousLv= new ArrayList<Triple>();
		for (QuerySolution qs:res) {
			Triple triple = new Triple (qs.get("?Concept").toString(),"rdfs:subClassOf",q);
			//System.out.println("Class axioms: "+triple+"i: "+i);
			triplePreviousLv.add(triple);

		}

		inclusions.addAll(triplePreviousLv); //add to the result


	for (int k =0 ; k<depth-1;k++) {

			tripleofCurrentLevel= new ArrayList<Triple>();
			for (Triple triple:triplePreviousLv) {
				List<QuerySolution> executeQuery = qe.executeQuery(composeQuery(triple.getS()));
				for (QuerySolution qs:executeQuery) {
					Triple t = new Triple (qs.get("?Concept").toString(),"rdfs:subClassOf",triple.getS());
					System.out.println("Class axioms: "+t);
					tripleofCurrentLevel.add(t);

				}

			}
			inclusions.addAll(tripleofCurrentLevel);
			triplePreviousLv=tripleofCurrentLevel;
		
	
		}		
	}


	private String composeQuery(String q) {
		String where= q.compareTo("owl:Thing")==0?"WHERE { ?Concept rdfs:subClassOf "+q+"}":"WHERE { ?Concept rdfs:subClassOf <"+q+">}" ; 
		String limit= nOfClasses==0?"":"LIMIT "+nOfClasses; 
		String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> SELECT DISTINCT *"+where+limit;
		System.out.println("************"+query);
		return query;
	}

	public boolean checkDisjointness(String c1, String c2) {
		String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> "
				+"SELECT DISTINCT ?x where { ?x a <"+c1+">. ?x  a <"+c2+">.}";
		// todo: vedere se il numero di triple � zero
		System.out.println(query);
		List<QuerySolution> res = qe.executeQuery(query);
		System.out.println("Disjoint? "+ (res.size()==0));
		return  res.size()==0;


	} 



}
