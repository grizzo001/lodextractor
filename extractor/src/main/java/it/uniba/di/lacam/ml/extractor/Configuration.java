package it.uniba.di.lacam.ml.extractor;

import java.net.URLEncoder;

import it.uniba.di.lacam.ml.extractor.utils.KnowledgeGraph;

/**
 * A class for reading the parameters from cli 
 * @author Giuseppe
 *
 */

public class Configuration {

	
	
	public static int  nOfClasses =10;
	public static int depth=2; // per classes
	public static int nOfInstances=5; //per classes
	public static int nOfDataProperties =5; // per classes
	public static int nOfObjectProperties=5; // per classes
	public static int nOfObjectPropertiesAssertions= 5; // per properties
	
	public static boolean online = true;
	public static String url="https://dbpedia.org/sparql";
	public static KnowledgeGraph kg= KnowledgeGraph.DBPEDIA;
	public static String fileIRI="file:////C:/Users/Giuseppe/Desktop/Ontologies/ontoEukaryot.owl";
	public static int nOfDataPropertiesAssertion=10;
	public static String topConcept= "http://dbpedia.org/ontology/Eukaryote"; //"owl:Thing"; 
						//"http://dbpedia.org/ontology/Work";
	public Configuration() {}


		
	}


