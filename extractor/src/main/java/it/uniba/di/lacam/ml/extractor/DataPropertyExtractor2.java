package it.uniba.di.lacam.ml.extractor;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.Literal;

import it.uniba.di.lacam.ml.extractor.utils.QueryExecutor;
import it.uniba.di.lacam.ml.extractor.utils.Triple;

/**
 * A class to extract information about object properties 
 * @author Giuseppe
 *
 */
public class DataPropertyExtractor2 {

	private QueryExecutor  qe ;

	int maxDProps;
	
	int maxDPropsAss;
	/**
	 * 
	 * @param qe, the query executor
	 * @param nOfDataProperties, maximum number of data properties
	 * @param nOfDataPropertiesAssertion, per dataproperty
	 */
	public DataPropertyExtractor2(QueryExecutor qe, int nOfDataProperties, int nOfDataPropertiesAssertion) {
		this.qe=qe;
	maxDProps= nOfDataProperties;
	maxDPropsAss= nOfDataPropertiesAssertion;
	}

	/**
	 * Extract the classes in the domain
	 * @param inclusions
	 * @param q
	 * @return
	 */
	public List<Triple> retrievepropertiesDomain( List<Triple> inclusions, String q){
		String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> "
				+ "select distinct ?p  where {\r\n" + 
				"?p a owl:DatatypeProperty."+
				" ?p rdfs:domain"+q+" \r\n" + 
				"}";

		//System.out.println(query);
		List<Triple>  executeQuery=  new ArrayList<Triple>();
		List<QuerySolution> res=qe.executeQuery(query);
		// System.out.println("Results: "+res.size());
		for (QuerySolution querySolution : res) {


			Triple t= new Triple((querySolution.getResource("?p").toString()),"rdfs:domain", q);
			executeQuery.add(t);

		}

		inclusions.addAll(executeQuery);
		//System.err.println(executeQuery.size());
		return executeQuery;



	}

	/**
	 * Extract the information about ranges
	 * @param inclusions
	 * @param q
	 * @return
	 */
	public List<Triple> retrieveproperties( List<Triple> inclusions, String q){
		String q1 = q.compareTo("owl:Thing")!=0?"<"+q+">": q;
		String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> "
				+"PREFIX xsd:  <http://www.w3.org/2001/XMLSchema#>"+ 
				"select distinct ?p ?s where {\r\n" 
				+ "?p a owl:DatatypeProperty."+
				" ?p rdfs:domain "+q1+"."
				+ "?p rdfs:range ?s}  LIMIT "+maxDProps;

		//System.out.println(query);
		List<Triple>  executeQuery=  new ArrayList<Triple>();
		List<QuerySolution> res=qe.executeQuery(query);
		//System.out.println("Results: "+res.size());
		for (QuerySolution querySolution : res) {


			Triple t= new Triple(q,querySolution.getResource("?p").toString(), querySolution.getResource("?s").toString());
			executeQuery.add(t);

		}

		inclusions.addAll(executeQuery);
		//System.err.println(executeQuery.size());
		return executeQuery;



	}

	/**
	 * Extract information about the property
	 * @param inclusions
	 * @param q
	 * @return
	 */
	public List<Triple> retrieveDPpropertiesAssertions(String q){
		String q1 = q.compareTo("owl:Thing")!=0?"<"+q+">": q;
		String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> "
				+ "select ?s ?o where {\r\n" + 
				"?s  "+q1+"  ?o. \r\n"+
				"} LIMIT "+ maxDPropsAss;

		//System.out.println(query);
		List<Triple>  executeQuery=  new ArrayList<Triple>();
		List<QuerySolution> res=qe.executeQuery(query);
		//System.out.println("Results: "+res.size());
		for (QuerySolution querySolution : res) {
			Literal literal = querySolution.getLiteral("?o");
			String string = literal.toString();
			if (!string.contains("^^"))
			string=string+"^^"+literal.getDatatypeURI();
			
			
			Triple t= new Triple((querySolution.getResource("?s").toString()), q,string);
			executeQuery.add(t);

		}

		//(inclusions.addAll(executeQuery);
		//System.err.println(executeQuery.size());
		return executeQuery;



	}




}
