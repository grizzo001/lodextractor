package it.uniba.di.lacam.ml.extractor;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.ResultSet;

import it.uniba.di.lacam.ml.extractor.utils.QueryExecutor;
import it.uniba.di.lacam.ml.extractor.utils.Triple;

import org.apache.jena.query.QuerySolution;
/**
 * A class to extract the resources used as individuals
 * @author Giuseppe Rizzo
 *
 */
public class InstanceExtractor {
	
	private QueryExecutor  qe ;
	private int ninstances; // per class
	private int depth;
	// for recursive queries
	/**
	 * 
	 * @param qe, the executor
	 * @param ninstances, maximum number of individuals per class
	 */
	public InstanceExtractor(QueryExecutor qe, int ninstances) {
	this.qe=qe;
	this.ninstances=ninstances;
	//this.depth= depth;
	}

	/**
	 * retrieve the direct instances of a class
	 * @param aClass
	 * @return triple involving individuals
	 */
public List<Triple> retrieveInstances(String aClass) {
		
		String cl= aClass.compareTo("owl:Thing")!=0? "<"+aClass+">": aClass;
		String limit= (ninstances==0)?"":"LIMIT "+ninstances;
		System.out.println(limit);
		String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> SELECT DISTINCT ?s WHERE { ?s a "+cl +"} "+limit;
		System.out.println("   *"+query);
		List<QuerySolution> res= qe.executeQuery(query);
		List<Triple>  inclusions= new ArrayList<Triple>();
		for (int j = 0; j < res.size(); j++) {
			Triple triple = new Triple (res.get(j).get("?s").toString(),"a",aClass);
			//System.out.println("Triple "+triple);
			inclusions.add( triple);
		//	retrieveSubClassesTriples(inclusions, res.get(j).get("?Concept").toString(), (i+1));	
		}
		return inclusions;
	} 
}


