package it.uniba.di.lacam.ml.extractor;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
/**
 * An utility class to store strings
 * @author Giuseppe
 *
 */
public class Messages {
	private static final String BUNDLE_NAME = "it.uniba.di.lacam.ml.extractor.messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	private Messages() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
