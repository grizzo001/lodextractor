package it.uniba.di.lacam.ml.extractor;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.QuerySolution;

import it.uniba.di.lacam.ml.extractor.utils.QueryExecutor;
import it.uniba.di.lacam.ml.extractor.utils.Triple;

/**
 * A class to extract information about object properties 
 * @author Giuseppe
 *
 */
public class ObjectPropertyExtractor {

	private QueryExecutor  qe ;
	private int maxProp;
	private int maxPropAss;
	
	public ObjectPropertyExtractor(QueryExecutor qe, int nOfProp, int nOfAssertions) {
	 this.qe=qe;
	maxProp=nOfProp;
	maxPropAss=nOfAssertions;
	}
	
	
	/**
	 * Extract the classes in the domaain
	 * @param inclusions
	 * @param q
	 * @return
	 */
	 public List<Triple> retrieveproperties( List<Triple> inclusions, String d, String r){
		String d1 = d.compareTo("owl:Thing")!=0?"<"+d+">": d;
		String r1 = r.compareTo("owl:Thing")!=0?"<"+r+">": r; 
		 String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> "
		 		+ "select  distinct ?p where {\r\n" + 
		 		"?p a owl:ObjectProperty."+
		 		" ?p rdfs:domain "+d1+". \r\n" +
		 		" ?p rdfs:range "+r1+". \r\n" +
		 		"} LIMIT "+maxProp;
		 
		 System.out.println("*********"+query);
		 List<Triple>  executeQuery=  new ArrayList<Triple>();
		 List<QuerySolution> res=qe.executeQuery(query);
			 //System.out.println("Results: "+res.size());
			for (QuerySolution querySolution : res) {
				
			
			Triple t= new Triple(d,(querySolution.getResource("?p").toString()),r);
			//System.out.println("ob triple: "+t+ " added");
			executeQuery.add(t);
		
		}
		
			inclusions.addAll(executeQuery);
			//System.err.println(executeQuery.size());
			return executeQuery;
		
		
		
	}
	 
//	 /**
//	  * Extract the information about ranges
//	  * @param inclusions
//	  * @param q
//	  * @return
//	  */
//	 public List<Triple> retrievepropertiesRange( List<Triple> inclusions, String q){
//		 String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> "
//		 		+ "select distinct ?p where {\r\n" + 
//				 "?p a owl:ObjectProperty."+
//		 		"  ?p rdfs:range"+q+" \r\n" + 
//		 		"}";
//		 
//		 //System.out.println(query);
//		 List<Triple>  executeQuery=  new ArrayList<Triple>();
//		 List<QuerySolution> res=qe.executeQuery(query);
//			 //System.out.println("Results: "+res.size());
//			for (QuerySolution querySolution : res) {
//				
//			
//			Triple t= new Triple((querySolution.getResource("?p").toString()),"rdfs:range", q);
//			executeQuery.add(t);
//		
//		}
//		
//			inclusions.addAll(executeQuery);
//			//System.err.println(executeQuery.size());
//			return executeQuery;
//		
//		
//		
//	}
//	 
//	 /**
//	  * Extract information about the property
//	  * @param inclusions
//	  * @param q
//	  * @return
//	  */
	 /**
	  * Retrieve the links/properties between two resources
	  * @param s, the subject
	  * @param o, the object
	  * @return
	  */
	 public List<Triple> retrievepropertiesAssertions( String s, String o){
		 String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> "
		 		+ "select distinct ?p  where {\r\n <" + 
				  s+"> ?p <"+o+">.} LIMIT "+ maxPropAss;
		 
		 System.out.println(query);
		 List<Triple>  executeQuery=  new ArrayList<Triple>();
		 List<QuerySolution> res=qe.executeQuery(query);
			 System.out.println("Results: "+res.size());
			for (QuerySolution querySolution : res) {
			Triple t= new Triple(s,(querySolution.getResource("?p").toString()) , o);
			executeQuery.add(t);
		
		}
		
			//inclusions.addAll(executeQuery);
			//System.err.println(executeQuery.size());
			return executeQuery;
		
		
//		
	}
	 
	 public List<Triple> retrievepropertiesAssertions( String s){
		 String query ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX owl:<http://www.w3.org/2002/07/owl#> "
		 		+ "select distinct ?s ?o where {\r\n " + 
				   "?s <"+s+"> ?o.} LIMIT "+ maxPropAss;
		 
		 System.out.println(query);
		 List<Triple>  executeQuery=  new ArrayList<Triple>();
		 List<QuerySolution> res=qe.executeQuery(query);
			 System.out.println("Results: "+res.size());
			for (QuerySolution querySolution : res) {
				Triple t= new Triple(querySolution.getResource("?s").toString(),s, querySolution.getResource("?o").toString());
				executeQuery.add(t);
			}
		
			//inclusions.addAll(executeQuery);
			//System.err.println(executeQuery.size());
			return executeQuery;
		
		
//		
	}
	 
	 
	

}
