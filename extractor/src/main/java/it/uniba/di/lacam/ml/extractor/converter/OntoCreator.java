package it.uniba.di.lacam.ml.extractor.converter;



import java.util.Spliterator;
import java.util.StringTokenizer;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.SetOntologyID;
import org.semanticweb.owlapi.util.SimpleIRIMapper;
import it.uniba.di.lacam.ml.extractor.utils.NameExtractor;
import uk.ac.manchester.cs.owl.owlapi.OWL2DatatypeImpl;


public class OntoCreator {
private OWLOntologyManager manager;
private IRI fileIRI;
private  OWLOntology ontology;
private NameExtractor ne;
	

public OntoCreator(String kb, NameExtractor ne) {
		// TODO Auto-generated constructor stub
	fileIRI= IRI.create(kb);
	try {
		create();
	} catch (OWLOntologyCreationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	this.ne= ne;
	}


	@SuppressWarnings("deprecation")
	public void create() throws OWLOntologyCreationException {
		manager = OWLManager.createOWLOntologyManager();

		//IRI ontologyIRI = IRI.create("file://www.myontology.com//onto.owl");
		
		ontology = manager.createOntology(fileIRI);
		System.out.println("Created ontology: " + ontology);
		// OWLOntologyID ontologyID = ontology.getOntologyID();
		// In this case our ontology has an IRI but does not have a version IRI

		//SimpleIRIMapper mapper = new SimpleIRIMapper(ontologyIRI, fileIRI);
		//manager.addIRIMapper(mapper);



		
		//  OWLOntology ontology2 = manager.createOntology(ontologyIRI);

		//addSubsumptionAxiom(manager, fileIRI, ontology, fileIRI+"#classA", fileIRI+"#classB");
		//addClassAssertion(manager, fileIRI, ontology, fileIRI+"#a", fileIRI+"#classB");
		//addObjectProperty(manager, fileIRI, ontology, fileIRI+"#classA",fileIRI+"#prop1" , fileIRI+"#classA");
		//save(manager, fileIRI, ontology);

	}


	public void save() {
		try {
			manager.saveOntology(ontology, fileIRI);
		} catch (OWLOntologyStorageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void addSubsumptionAxiom( String iri1, String iri2 ) {
		String classA, classB;
		
		classA = ne.extractName(iri1);
		classB = ne.extractName(iri2); 
		System.out.println(classA);
		System.out.println(classB);

		OWLDataFactory factory = manager.getOWLDataFactory();

		OWLClass clsA = factory.getOWLClass(IRI.create(classA));
		OWLClass clsB = classB.equals("owl:Thing")?factory.getOWLThing():factory.getOWLClass(IRI.create(classB));
		// Now create the axiom
		OWLAxiom axiom = factory.getOWLSubClassOfAxiom(clsA, clsB);
		System.out.println("New Axiom:"+ axiom);
		AddAxiom addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);
	}


	

	public void addClassAssertion(String iri1, String iri2 ) {
		String ind, classB;
		ind= ne.extractName(iri1);
		classB= ne.extractName(iri2);; 
		System.out.println(ind);
		System.out.println(classB);

		OWLDataFactory factory = manager.getOWLDataFactory();

		OWLNamedIndividual clsA = factory.getOWLNamedIndividual(IRI.create(ind));
		OWLClass clsB = classB.equals("owl:Thing")?factory.getOWLThing():factory.getOWLClass(IRI.create(classB));
		//OWLClass clsB = factory.getOWLClass(IRI.create(/*fileIRI + "#"+*/classB));
		// Now create the axiom
		OWLAxiom axiom = factory.getOWLClassAssertionAxiom(clsB, clsA);//(clsA, clsB);
		AddAxiom addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);
	}


	public void addObjectProperty(String d, String p,String o) {
		String dom, prop, range;
		dom= ne.extractName(d);
		prop= ne.extractName(p); 
		range= ne.extractName(o);
		System.out.println(dom +"     "+prop+ "     "+range);
		//System.out.println(prop);

		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLObjectProperty objProp= factory.getOWLObjectProperty(IRI.create(prop));

		//domain axiom
		OWLClass domain =factory.getOWLClass(IRI.create(dom));

		OWLAxiom axiom = factory.getOWLObjectPropertyDomainAxiom(objProp, domain);
		
		AddAxiom addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);

		//RANGE AXIOM
		OWLClass rangeClass =factory.getOWLClass(IRI.create(range));

		OWLAxiom axiom2 = factory.getOWLObjectPropertyRangeAxiom(objProp, rangeClass);
		addAxiom = new AddAxiom(ontology, axiom2);
		
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);

	}
	
	public void addObjectPropertyAssertion(String d, String p,String o) {
		String dom, prop, range;
		dom= ne.extractName(d);
		prop= ne.extractName(p); 
		range= ne.extractName(o);
		System.out.println(dom +"     "+prop+ "     "+range);
		//System.out.println(prop);

		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLObjectProperty objProp= factory.getOWLObjectProperty(IRI.create(prop));

		
		OWLNamedIndividual ind1= factory.getOWLNamedIndividual(IRI.create(dom));
		OWLNamedIndividual ind2= factory.getOWLNamedIndividual(IRI.create(range));
		

		OWLAxiom axiom = factory.getOWLObjectPropertyAssertionAxiom(objProp, ind1, ind2);
		
		AddAxiom addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);


	}

	
	public void addDataPropertyAssertion(String d, String p,String o) {
		String dom, prop, range;
		dom= ne.extractName(d);
		prop= ne.extractName(p); 
		range= ne.extractName(o);
		System.out.println("DP "+dom +"     "+prop+ "     "+ range);

		StringTokenizer tk = new StringTokenizer(range, "^^");
		
		System.out.println("****"+range +" "+tk.countTokens());
		String value=tk.nextToken(); //split[0];
		String type=  tk.nextToken();//split[1];
		
		//System.out.println(prop);

		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLDataProperty dpProp= factory.getOWLDataProperty(IRI.create(prop));

		
		
		OWLNamedIndividual ind1= factory.getOWLNamedIndividual(IRI.create(dom));
		//OWLNamedIndividual ind2= factory.getOWLNamedIndividual(IRI.create(fileIRI + "#"+range));
	 
		OWLDatatype rangeClass =factory.getOWLDatatype(IRI.create(type));
		
			OWLLiteral l= factory.getOWLLiteral(value,rangeClass);
		//factory.getOWLDataPropertyAssertionAxiom(objProp, subject, object)
		OWLAxiom axiom = factory.getOWLDataPropertyAssertionAxiom(dpProp, ind1, l);
		
		AddAxiom addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);


	}

	
	public void addObjectPropertyDomain(String d, String p) {
		String dom, prop, range;
		dom = ne.extractName(d);
		prop = ne.extractName(p);
		
		
		//dom = d.split("#")[1].replace("<", "").replace(">","");
		//prop= p.split("#")[1].replace("<", "").replace(">",""); 
		//range= o.split("#")[1].replace("<", "").replace(">","");
		System.out.println(dom);
		System.out.println(prop);

		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLObjectProperty objProp= factory.getOWLObjectProperty(IRI.create(prop));

		//domain axiom
		OWLClass domain =factory.getOWLClass(IRI.create(dom));

		OWLAxiom axiom = factory.getOWLObjectPropertyDomainAxiom(objProp, domain);
		AddAxiom addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);

		//RANGE AXIOM
		//OWLClass rangeClass =factory.getOWLClass(IRI.create(fileIRI + "#"+dom));

		//axiom = factory.getOWLObjectPropertyRangeAxiom(objProp, rangeClass);
		//addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		//manager.applyChange(addAxiom);

	}


	/*public void addObjectPropertyRange(String d, String p) {
		String dom, prop, range;
		String[] split = d.split("#");
		dom= split.length==2?split[1].replace("<", "").replace(">",""):d;
		String[] split2 = p.split("#");
		prop= split2.length==2?split2[1].replace("<", "").replace(">",""):p;
		
		
		//dom = d.split("#")[1].replace("<", "").replace(">","");
		//prop= p.split("#")[1].replace("<", "").replace(">",""); 
		//range= o.split("#")[1].replace("<", "").replace(">","");
		System.out.println(dom);
		System.out.println(prop);

		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLObjectProperty objProp= factory.getOWLObjectProperty(IRI.create(fileIRI + "#"+prop));

		//domain axiom

		//RANGE AXIOM
		OWLClass rangeClass =factory.getOWLClass(IRI.create(fileIRI + "#"+dom));

		OWLAxiom axiom = factory.getOWLObjectPropertyRangeAxiom(objProp, rangeClass);
		AddAxiom addAxiom = new AddAxiom(ontology, axiom);
		 //We now use the manager to apply the change
		manager.applyChange(addAxiom);

	}*/

	
	public void addDataProperty(String d, String p,String o) {
		String dom, prop, range;
		dom= ne.extractName(d);
		prop= ne.extractName(p); //p.split("#")[1].replace("<", "").replace(">",""); 
		//range= o.split("#")[1].replace("<", "").replace(">","");
		System.out.println(dom);
		System.out.println(prop);

		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLDataPropertyExpression objProp= factory.getOWLDataProperty(IRI.create(prop));

		//domain axiom
		OWLClass domain =factory.getOWLClass(IRI.create(dom));

		OWLAxiom axiom = factory.getOWLDataPropertyDomainAxiom(objProp, domain);
		AddAxiom addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);

		//RANGE AXIOM
		OWLDatatype rangeClass =factory.getOWLDatatype(IRI.create(o));

		axiom = factory.getOWLDataPropertyRangeAxiom(objProp, rangeClass);
		System.out.println("New Axiom: "+axiom);
		addAxiom = new AddAxiom(ontology, axiom);
		// We now use the manager to apply the change
		manager.applyChange(addAxiom);

	}

public void addDisjointness(String iri1, String iri2) {
	
	OWLDataFactory factory = manager.getOWLDataFactory();
	
	
	String c = ne.extractName(iri1);
	String d = ne.extractName(iri2); 
	OWLClass class1 = factory.getOWLClass(IRI.create(/*fileIRI + "#"+*/c));
	OWLClass class2 = factory.getOWLClass(IRI.create(/*fileIRI + "#"+*/d));
	
	OWLAxiom ax=factory.getOWLDisjointClassesAxiom(class1, class2);
	
	AddAxiom addAxiom = new AddAxiom(ontology, ax);
	// We now use the manager to apply the change
	manager.applyChange(addAxiom);
	
}

}