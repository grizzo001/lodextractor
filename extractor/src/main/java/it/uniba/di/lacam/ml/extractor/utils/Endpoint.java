package it.uniba.di.lacam.ml.extractor.utils;

/**
 * Configuration of a sparqlEndpoint
 * @author Giuseppe Rizzo
 *
 */
public class Endpoint {
	
	private String endpoint="";
	public Endpoint(String url){
		endpoint= url;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}


}
