package it.uniba.di.lacam.ml.extractor.utils;
/**
 * Supported KGs 
 * @author Giuseppe Rizzo
 *
 */
public enum KnowledgeGraph {
	DBPEDIA,
	YAGO,
	MUSICBRAINZ,
	OTHER;
	

}
