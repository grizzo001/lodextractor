package it.uniba.di.lacam.ml.extractor.utils;
/**
 * Parse an iri to extract the resource name(without its namespace)
 * @author Giuseppe
 *
 */
public class NameExtractor {
	KnowledgeGraph kg;

	public NameExtractor( KnowledgeGraph kg) {
		this.kg= kg;
	}


 public String extractName(String iri) {
	 if (kg.compareTo(KnowledgeGraph.OTHER)==0)
	  return parseName(iri);
	 
	 if (kg.compareTo(KnowledgeGraph.YAGO)==0)
		  return parseNameYago(iri);
	 
	 if (kg.compareTo(KnowledgeGraph.DBPEDIA)==0)
		  return parseNameDBPedia(iri);
 
	 
	 return "";
	 
	 
	 
 }

/**
 * A parser for iri in the form <url>#<resource>
 * @param iri1
 * @return
 */
 private String parseName(String iri1) {
		String classA;
		String[] split = iri1.split("#");
		classA= split.length==2?split[1].replace("<", "").replace(">",""):iri1;
		return classA;
	}

 private String parseNameYago(String iri) {
		String classA= iri.replace("http://yago-knowledge.org/resource/","");
		return classA;
		//String[] split = iri1.rep;
		//classA= split.length==2?split[1].replace("<", "").replace(">",""):iri1;
		//return classA;
	}

 
 private String parseNameDBPedia(String iri) {
		String classA=  iri.replace("http://dbpedia.org/ontology/","");
		String classB=classA.replace("http://dbpedia.org/resource/","");
		//System.out.println("After parsing:"+classA);
		return classB;
		//String[] split = iri1.rep;
		//classA= split.length==2?split[1].replace("<", "").replace(">",""):iri1;
		//return classA;
	}

}
