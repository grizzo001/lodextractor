package it.uniba.di.lacam.ml.extractor.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Query;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
/**
 * A wrapper class to query a KG.
 * @author Giuseppe
 *
 */
public class QueryExecutor {
	Endpoint ep;
	QueryExecution qexec;// 
	public static boolean online=true;
	Model model;
	public QueryExecutor(Endpoint ep, boolean o) {
		this.ep=ep; 

		online=o;
		if (!online) {
			model = ModelFactory.createDefaultModel();
		model.read(ep.getEndpoint());
		}
	} 

	public List<QuerySolution> executeQuery(String queryString) {
	
		

		Query query = QueryFactory.create(queryString) ;
		String endpoint = ep.getEndpoint();
		//QueryExecutionFactory.sparqlService(endpoint, query);
		QueryExecution qexec =  QueryExecutionFactory.sparqlService(endpoint, query); 
		//qexec.isClosed();
	    System.out.println("xxxxxxxxxxxxxx" + qexec.isClosed());
		ResultSet results = qexec.execSelect();
		//System.out.println("??????????????????");
		List<QuerySolution> qs= results.hasNext()?ResultSetFormatter.toList(results): new ArrayList<QuerySolution>();
		qexec.close();
		return qs;

	}
}