package it.uniba.di.lacam.ml.extractor.utils;

public class Triple {


	String s, p, o;
	public Triple(String s, String p, String o) {
		this.s = s;
		this.p = p;
		this.o = o;
		
		
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	public String getP() {
		return p;
	}
	public void setP(String p) {
		this.p = p;
	}
	public String getO() {
		return o;
	}
	public void setO(String o) {
		this.o = o;
	}

	public String toString() {
		return "("+s+","+p+","+o+")";
	}
}
