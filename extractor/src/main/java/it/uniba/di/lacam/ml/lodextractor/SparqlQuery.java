package it.uniba.di.lacam.ml.lodextractor;


import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFNode;
//import at.jku.dke.hilal.analysis_graphs.DimensionsToAnalysisSituation;
//import at.jku.dke.hilal.md_elements.Dimension;
//import at.jku.dke.hilal.owl_handler.BasicOWLHandler;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.ontology.Individual;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.jena.base.Sys;
import org.apache.jena.graph.Node;
import org.apache.jena.sparql.syntax.ElementTriplesBlock;

import it.uniba.di.lacam.ml.extractor.ClassesExtractor;
import it.uniba.di.lacam.ml.extractor.Configuration;
import it.uniba.di.lacam.ml.extractor.DataPropertyExtractor2;
import it.uniba.di.lacam.ml.extractor.InstanceExtractor;
import it.uniba.di.lacam.ml.extractor.ObjectPropertyExtractor;
import it.uniba.di.lacam.ml.extractor.converter.OntoCreator;
import it.uniba.di.lacam.ml.extractor.utils.Endpoint;
import it.uniba.di.lacam.ml.extractor.utils.KnowledgeGraph;
import it.uniba.di.lacam.ml.extractor.utils.NameExtractor;
import it.uniba.di.lacam.ml.extractor.utils.QueryExecutor;
/**
 * An it.uniba.di.lacam.ml.extractor of a fragment from KG properly converted into an OWL ontology 
 * @author Giuseppe Rizzo
 *
 */
public class SparqlQuery extends Query {    

	public static void main (String [] args) throws UnsupportedEncodingException{                   
		
		
		if (args.length>0) {
			Configuration.online=Boolean.parseBoolean(args[0]);
			Configuration.url=args[1];
			Configuration.nOfClasses=Integer.parseInt(args[2]);
			Configuration.depth= Integer.parseInt(args[3]);
			Configuration.nOfInstances=Integer.parseInt(args[4]);
			Configuration.nOfObjectProperties=Integer.parseInt(args[5]);
			Configuration.nOfObjectPropertiesAssertions=Integer.parseInt(args[6]);
			Configuration.nOfDataProperties= Integer.parseInt(args[7]);
			Configuration.nOfDataPropertiesAssertion=Integer.parseInt(args[8]);
			Configuration.kg=KnowledgeGraph.valueOf(args[0]);
			Configuration.fileIRI=args[10];
		} // default value 

		//obtaining 1000 classes;
		int k= 0;
		Endpoint ep= new Endpoint//("C:/Users/Giuseppe/Documents/ontos/biopax.owl"); 
				(Configuration.url);
		//System.out.println(Configuration.url);
		//("https://linkeddata1.calcul.u-psud.fr/sparql");
		//("https://linkeddata1.calcul.u-psud.fr/sparql");
		QueryExecutor qe= new QueryExecutor(ep, Configuration.online);
		NameExtractor ne= new  NameExtractor(Configuration.kg);



		List<it.uniba.di.lacam.ml.extractor.utils.Triple> retrieveClasses = new ArrayList<it.uniba.di.lacam.ml.extractor.utils.Triple>();
		List<it.uniba.di.lacam.ml.extractor.utils.Triple> retrieveSubClasses = new ArrayList<it.uniba.di.lacam.ml.extractor.utils.Triple>();               
		ClassesExtractor ce= new ClassesExtractor(qe, Configuration.nOfClasses, Configuration.depth);
		//retrieveClasses.addAll(ce.retrieveSubClassesTriples());
		ce.retrieveSubClassesTriples(retrieveSubClasses);
		//System.out.println(queryString);
		//retrieveSubClasses.addAll(retrieveClasses);
		System.out.println("Number of classes: "+retrieveSubClasses.size());
		retrieveSubClasses.forEach((f)->System.out.println(f));
		//List<QuerySolution> retrieveClasses = ce.retrieveClasses();


		ObjectPropertyExtractor obe= new ObjectPropertyExtractor(qe, Configuration.nOfObjectProperties,Configuration.nOfObjectPropertiesAssertions);
		DataPropertyExtractor2 de=new DataPropertyExtractor2(qe, Configuration.nOfDataProperties,Configuration.nOfDataPropertiesAssertion);
		List<it.uniba.di.lacam.ml.extractor.utils.Triple> objProps= new ArrayList<it.uniba.di.lacam.ml.extractor.utils.Triple>();
		List<it.uniba.di.lacam.ml.extractor.utils.Triple> objPropsAss= new ArrayList<it.uniba.di.lacam.ml.extractor.utils.Triple>();		
		List<it.uniba.di.lacam.ml.extractor.utils.Triple> dProps= new ArrayList<it.uniba.di.lacam.ml.extractor.utils.Triple>();
		List<it.uniba.di.lacam.ml.extractor.utils.Triple> dpPropsAss= new ArrayList<it.uniba.di.lacam.ml.extractor.utils.Triple>();


		InstanceExtractor ie= new InstanceExtractor(qe, Configuration.nOfInstances);
		System.out.println("RETRIEVE INSTANCES....."+Configuration.nOfInstances);
		HashSet<String> classes= getClassesSignature(retrieveSubClasses);
		HashSet<it.uniba.di.lacam.ml.extractor.utils.Triple> retrieveInstances = retrieveInstances(classes, ie);
		//
		//		// prendere le sottoclassi  e confrontarli a coppie
		//		//; //extract all the classes
		for (String c: classes) {
			de.retrieveproperties(dProps, c); // retrieve properties 
			for (String d: classes) {
				obe.retrieveproperties(objProps, c, d); //retrieve object properties

			}

		}


		// extract obj props assertions 

		//de.retrievepropertiesAssertions(inclusions, q)


		for (it.uniba.di.lacam.ml.extractor.utils.Triple i: objProps) {
			String currentProp= i.getP();
			List<it.uniba.di.lacam.ml.extractor.utils.Triple> retrievepropertiesAssertions = obe.retrievepropertiesAssertions(currentProp);
			retrievepropertiesAssertions.removeIf((f)->retrieveInstances.contains(f.getS())&&retrieveInstances.contains(f.getO()));
			objPropsAss.addAll(retrievepropertiesAssertions); //update the set of properties asserions

		}
		for (it.uniba.di.lacam.ml.extractor.utils.Triple i: dProps) {
			String currentProp= i.getP();
			List<it.uniba.di.lacam.ml.extractor.utils.Triple> retrievepropertiesAssertions = de.retrieveDPpropertiesAssertions(currentProp);
			// retrievepropertiesAssertions.removeIf((f)->retrieveInstances.contains(f.getS()));
			dpPropsAss.addAll(retrievepropertiesAssertions); //update the set of properties asserions

		}





		// write triples as owl axiom
		OntoCreator  c= new OntoCreator(Configuration.fileIRI, ne);

		retrieveSubClasses.forEach((f)->{System.out.println(f); c.addSubsumptionAxiom(f.getS(), f.getO());});

		//System.out.println("#Classes"+retrieveClasses.size());

		retrieveInstances.forEach((f)->{System.out.println(f); c.addClassAssertion(f.getS(), f.getO());});


		//retrieveInstances.forEach((d)->System.out.println(d.getS()));

		//System.err.println( "#properties ranges for"+prop2.size());


		System.out.println("\n\n\nOBJECT PROPERTIES GENERATION");
		objProps.forEach((f)->{System.out.println(f); c.addObjectProperty(f.getS(), f.getP(), f.getO());});
		System.out.println("\n\n\nOBJECT PROPERTIES ASSERTION GENERATION");
		objPropsAss.forEach((f)->{c.addObjectPropertyAssertion(f.getS(), f.getP(), f.getO());});
		System.out.println("\n\n\n DATA PROPERTIES ASSERTION GENERATION");
		//
		dProps.forEach((f)->{System.out.println(f); c.addDataProperty(f.getS(), f.getP(), f.getO());});
		dpPropsAss.forEach((f)-> c.addDataPropertyAssertion(f.getS(), f.getP(), f.getO()));



		//for (int i = 0; i <retrieveSubClasses.size(); i++) {
		//System.out.println(retrieveSubClasses.get(i));
		//} 




		// write disjointness axioms

		System.out.println("\n\n\n");
		System.out.println("DISJOINTNESS AXIOMS GENERATION");

		for (String cl: classes) {

			for (String d: classes) {
				System.out.println(cl +"  "+d);

				boolean areDisjoint= cl.equals("owl:Thing")||d.equals("owl:Thing")? false:ce.checkDisjointness(cl, d);
				if (areDisjoint)
					c.addDisjointness(cl, d);


			}
		}


		c.save();

		System.out.println("Characteristics of generated ontology");
		System.out.print("#classes"+ classes.size());
		System.out.println("#Axioms: "+retrieveSubClasses.size());
		System.out.println("Number of properties "+objProps.size());
		System.out.println("#datatypeproperties : "+dProps.size());
		System.out.println("#datatypeproperties assertions: "+dpPropsAss.size());
		//System.out.println("#individuals"+ retrieveInstances.size());
		System.out.println();
		System.out.println("END. Saved in "+ Configuration.fileIRI);


	}

	private static HashSet<it.uniba.di.lacam.ml.extractor.utils.Triple> retrieveInstances(HashSet<String> signature,
			InstanceExtractor ie) {
		HashSet<it.uniba.di.lacam.ml.extractor.utils.Triple> retrieveInstances= new HashSet<>();
		for (String s: signature) {
			String  current =s; //retrieveSubClasses.get(i).getO();
			System.out.println();

			retrieveInstances.addAll(ie.retrieveInstances(current));

		}

		//		for (int i=0; i< retrieveSubClasses.size();i++) {
		//			String  current = retrieveSubClasses.get(i).getS();
		//			retrieveInstances.addAll(ie.retrieveInstances(current));
		//
		//		}
		return retrieveInstances;
	}

	private static HashSet<String> getClassesSignature(List<it.uniba.di.lacam.ml.extractor.utils.Triple> retrieveSubClasses) {
		HashSet<String> retrieveInstances= new HashSet<>();
		for (int i=0; i< retrieveSubClasses.size();i++) {
			String  current = retrieveSubClasses.get(i).getO();

			retrieveInstances.add(current);

		}

		for (int i=0; i< retrieveSubClasses.size();i++) {
			String  current = retrieveSubClasses.get(i).getS();
			retrieveInstances.add(current);

		}
		return retrieveInstances;
	}

}